﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace WebApplication.Models
{
    public class Test_model
    {
        public int PersonId { get; set; }

        [Display(Name = "Name")]
        public string PersonName { get; set; }

        [Display(Name ="Enter Age")]
        public int Age { get; set; }

        public List<Test_model> Test_ModelList { get; set; }
    }
}
