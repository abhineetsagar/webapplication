﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [HttpGet]
        public ActionResult FirstView()
        {
            Test_model _model = new Test_model();

            return View(_model);
        }


        //public ActionResult FirstView(FormCollection fc)
        //
        //    Test_model _model = new Test_model();
        //    ViewBag.Id = fc["Id"];
        //    ViewBag.Name = fc["Name"];
        //    bool chk = Convert.ToBoolean(fc["Addon"].Split(',')[0]);
        //    ViewBag.Addon = chk;
        //    return View(_model);
        //

        List<Test_model> modelList = new List<Test_model>(){

                    new Test_model() { PersonId = 1, PersonName = "John", Age = 18 },
                    new Test_model() { PersonId = 2, PersonName = "Steve", Age = 21 },
                    new Test_model() { PersonId = 3, PersonName = "Bill", Age = 25 },
                    new Test_model() { PersonId = 4, PersonName = "Ram", Age = 20 },
                    new Test_model() { PersonId = 5, PersonName = "Ron", Age = 31 },
                    new Test_model() { PersonId = 6, PersonName = "Chris", Age = 17 },
                    new Test_model() { PersonId = 7, PersonName = "Rob", Age = 19 } };

        [HttpGet]
        public ActionResult Index()
        {
            return View(modelList);
        }

        public ActionResult Edit(int Id)
        {
            //Get the student from studentList sample collection for demo purpose.
            //Get the student from the database in the real application
            var std = modelList.Where(s => s.PersonId == Id).FirstOrDefault();

            return View(std);
        }


        [HttpPost]
        public ActionResult Edit(Test_model std)
        {
            //write code to update student 
            
            return RedirectToAction("Index");
        }
    }
}